//Kevin Echenique Aroyo #1441258
package lab02Eclipse;

public class BikeStore {

	public static void main(String[] args) {
		Bicycle[] b = new Bicycle[4];//Will hold 4 bicycle objects
		b[0] = new Bicycle("BMW", 8, 60);
		b[1] = new Bicycle("Maserati", 10, 200);
		b[2] = new Bicycle("Smart", 1, 10);
		b[3] = new Bicycle("Meh", 5, 40);
		
		for(Bicycle bike : b) {
			System.out.println(bike);
		}

	}

}
